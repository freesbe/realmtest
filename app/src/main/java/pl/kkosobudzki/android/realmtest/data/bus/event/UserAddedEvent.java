package pl.kkosobudzki.android.realmtest.data.bus.event;

import pl.kkosobudzki.android.realmtest.data.model.User;

/**
 * @author krzysztof.kosobudzki
 */
public class UserAddedEvent {
    private final User mUser;

    public UserAddedEvent(final User user) {
        mUser = user;
    }

    public User getUser() {
        return mUser;
    }
}
