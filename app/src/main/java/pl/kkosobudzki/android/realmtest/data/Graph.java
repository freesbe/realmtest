package pl.kkosobudzki.android.realmtest.data;

import javax.inject.Singleton;

import dagger.Component;
import pl.kkosobudzki.android.realmtest.MainActivity;
import pl.kkosobudzki.android.realmtest.data.bus.BusModel;
import pl.kkosobudzki.android.realmtest.view.dialog.NewUserDialog;

/**
 * @author krzysztof.kosobudzki
 */
@Singleton
@Component(modules = { BusModel.class })
public interface Graph {
    void inject(NewUserDialog dialog);
    void inject(MainActivity activity);

    public final static class Initializer {
        public static Graph init() {
            return Dagger_Graph.builder()
                    .build();
        }
    }
}
