package pl.kkosobudzki.android.realmtest;

import android.app.Application;

import pl.kkosobudzki.android.realmtest.data.Graph;

/**
 * @author krzysztof.kosobudzki
 */
public class App extends Application {
    private static App sObj;

    private Graph mGraph;

    public void onCreate() {
        super.onCreate();

        sObj = this;

        mGraph = Graph.Initializer.init();
    }

    public static App getInstance() {
        return sObj;
    }

    public Graph getGraph() {
        return mGraph;
    }
}
