package pl.kkosobudzki.android.realmtest;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmResults;
import pl.kkosobudzki.android.realmtest.data.bus.event.UserAddedEvent;
import pl.kkosobudzki.android.realmtest.data.model.User;
import pl.kkosobudzki.android.realmtest.view.adapter.UsersAdapter;
import pl.kkosobudzki.android.realmtest.view.dialog.NewUserDialog;


public class MainActivity extends ActionBarActivity {
    @InjectView(R.id.new_user_button) Button mNewUserButton;
    @InjectView(R.id.users_recyler_view) RecyclerView mUsersRecyclerView;

    @Inject Bus mBus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        ButterKnife.inject(this);

        App.getInstance().getGraph().inject(this);

        mUsersRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mUsersRecyclerView.setAdapter(new UsersAdapter());

        Realm realm = Realm.getInstance(this);
        RealmResults<User> users = realm.allObjects(User.class);

        ((UsersAdapter) mUsersRecyclerView.getAdapter()).setUsers(users);
    }

    @Override
    protected void onResume() {
        super.onResume();

        mBus.register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        mBus.unregister(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.new_user_button)
    public void showDialogNewUser() {
        NewUserDialog newUserDialog = new NewUserDialog();
        newUserDialog.show(getFragmentManager(), NewUserDialog.class.getName());
    }

    @Subscribe
    public void userAdded(UserAddedEvent event) {
        ((UsersAdapter) mUsersRecyclerView.getAdapter()).addUser(event.getUser());

        mUsersRecyclerView.getAdapter().notifyDataSetChanged();
    }
}
