package pl.kkosobudzki.android.realmtest.data.bus;

import com.squareup.otto.Bus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * @author krzysztof.kosobudzki
 */
@Module
public class BusModel {

    @Provides
    @Singleton
    Bus provideBus() {
        return new Bus();
    }
}
