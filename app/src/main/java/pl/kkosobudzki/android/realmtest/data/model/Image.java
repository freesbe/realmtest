package pl.kkosobudzki.android.realmtest.data.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * @author krzysztof.kosobudzki 
 */
public class Image extends RealmObject {
    
    @PrimaryKey
    private long id;
    private String path;
    
    public Image() {
        
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
