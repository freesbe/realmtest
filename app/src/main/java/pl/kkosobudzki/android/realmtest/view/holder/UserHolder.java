package pl.kkosobudzki.android.realmtest.view.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import pl.kkosobudzki.android.realmtest.R;
import pl.kkosobudzki.android.realmtest.data.model.User;

/**
 * @author krzysztof.kosobudzki
 */
public class UserHolder extends RecyclerView.ViewHolder {
    @InjectView(R.id.item_text_view) TextView mNameTextView;

    public UserHolder(View itemView) {
        super(itemView);

        ButterKnife.inject(this, itemView);
    }

    public void bind(User user) {
        mNameTextView.setText(String.format("[%s] %s %s", user.getId(), user.getSurname(), user.getName()));
    }
}
