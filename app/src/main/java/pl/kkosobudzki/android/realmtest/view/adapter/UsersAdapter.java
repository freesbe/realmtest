package pl.kkosobudzki.android.realmtest.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import pl.kkosobudzki.android.realmtest.R;
import pl.kkosobudzki.android.realmtest.data.model.User;
import pl.kkosobudzki.android.realmtest.view.holder.UserHolder;

/**
 * @author krzysztof.kosobudzki
 */
public class UsersAdapter extends RecyclerView.Adapter<UserHolder> {
    private final List<User> mUsers = new ArrayList<User>();

    @Override
    public UserHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user, parent, false);

        return new UserHolder(view);
    }

    @Override
    public void onBindViewHolder(UserHolder holder, int position) {
        holder.bind(mUsers.get(position));
    }

    @Override
    public int getItemCount() {
        return mUsers.size();
    }

    public void setUsers(List<User> users) {
        mUsers.clear();
        mUsers.addAll(users);
    }

    public void addUser(User user) {
        mUsers.add(user);
    }
}
