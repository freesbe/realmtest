package pl.kkosobudzki.android.realmtest.view.util;

import android.content.Context;
import android.text.TextUtils;
import android.widget.EditText;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

/**
 * Validation helper.
 *
 * Use it to validate {@link android.widget.EditText} and invoke action if all fields are valid.
 * If there are invalid fields the individual error messages are set to them.
 *
 * @author krzysztof.kosobudzki
 */
public class ValidationHelper {
    private final Map<WeakReference<EditText>, String> mViews = new HashMap<WeakReference<EditText>, String>();

    private final WeakReference<Context> mContextReference;

    public ValidationHelper(final Context context) {
        mContextReference = new WeakReference<Context>(context);
    }

    /**
     * Ensure {@param editText} is not empty, if it is display {@param errorMsgResId}.
     *
     * @param editText
     * @param errorMsgResId
     * @throws java.lang.IllegalArgumentException if referenced {@code context} is null.
     * @return
     */
    public ValidationHelper notEmpty(final EditText editText, final int errorMsgResId) {
        Context context = mContextReference.get();

        if (context == null) {
            throw new IllegalArgumentException("Context is null");
        }

        mViews.put(new WeakReference<EditText>(editText), context.getString(errorMsgResId));

        return this;
    }

    /**
     * Validates attached views.
     *
     * When all fields are valid method callback of object {@param callbackIfValid} is invoked.
     *
     * @param callbackIfValid
     */
    public void validate(final CallbackIfValid callbackIfValid) {
        boolean hasErrors = false;

        for (Map.Entry<WeakReference<EditText>, String> entry : mViews.entrySet()) {
            EditText editText = entry.getKey().get();

            if (editText != null) {
                if (TextUtils.isEmpty(editText.getText().toString())) {
                    editText.setError(entry.getValue());

                    hasErrors = true;
                } else {
                    editText.setError(null);
                }
            }
        }

        if (!hasErrors && callbackIfValid != null) {
            callbackIfValid.callback();
        }
    }

    public static interface CallbackIfValid {
        void callback();
    }
}
