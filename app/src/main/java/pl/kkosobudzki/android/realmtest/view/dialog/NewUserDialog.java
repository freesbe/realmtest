package pl.kkosobudzki.android.realmtest.view.dialog;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;

import com.squareup.otto.Bus;

import java.util.UUID;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import hugo.weaving.DebugLog;
import io.realm.Realm;
import pl.kkosobudzki.android.realmtest.App;
import pl.kkosobudzki.android.realmtest.R;
import pl.kkosobudzki.android.realmtest.data.bus.event.UserAddedEvent;
import pl.kkosobudzki.android.realmtest.data.model.User;
import pl.kkosobudzki.android.realmtest.view.util.ValidationHelper;

/**
 * @author krzysztof.kosobudzki
 */
public class NewUserDialog extends DialogFragment {
    @InjectView(R.id.name_edit_text) EditText mNameEditText;
    @InjectView(R.id.surname_edit_text) EditText mSurnameEditText;

    @Inject Bus mBus;

    private ValidationHelper mValidationHelper;

    public NewUserDialog() {

    }

    @Override
    public void onStart() {
        super.onStart();

        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.dialog_new_user, container, true);

        ButterKnife.inject(this, v);

        mValidationHelper = new ValidationHelper(getActivity())
            .notEmpty(mNameEditText, R.string.dialog_new_user_error_name)
            .notEmpty(mSurnameEditText, R.string.dialog_new_user_error_surname);

        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        App.getInstance().getGraph().inject(this);
    }

    @OnClick(R.id.dialog_cancel)
    public void cancel() {
        dismiss();
    }

    @OnClick(R.id.dialog_add)
    @DebugLog
    public void add() {
        mValidationHelper.validate(new ValidationHelper.CallbackIfValid() {
            @Override
            public void callback() {
                Realm realm = Realm.getInstance(getActivity());

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        User user = realm.createObject(User.class);
                        user.setId(UUID.randomUUID().toString());
                        user.setName(mNameEditText.getText().toString());
                        user.setSurname(mSurnameEditText.getText().toString());

                        mBus.post(new UserAddedEvent(user));
                    }
                });

                dismiss();
            }
        });
    }
}
