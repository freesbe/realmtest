package pl.kkosobudzki.android.realmtest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * @author krzysztof.kosobudzki
 */
@Config(emulateSdk = 18, manifest = "app/src/main/AndroidManifest.xml")
@RunWith(RobolectricTestRunner.class)
public class ActivityTest {
    
    @Test
    public void test_shouldHaveApplicationName() {
        String appName = new MainActivity().getResources().getString(R.string.app_name);
        
        assertThat(appName, equalTo("realmtest"));
    }
}
