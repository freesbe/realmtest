Test of library Realm
================
Android project using [Realm database](http://realm.io/). 
This project tests basic usage of library.


Other libraries used in project:

1. [Butter Knife](http://jakewharton.github.io/butterknife/)
2. [Hugo](https://github.com/JakeWharton/hugo)
3. [RecyclerView](https://developer.android.com/training/material/lists-cards.html)
4. [Dagger 2](http://google.github.io/dagger/)
5. [Otto EventBus](http://square.github.io/otto/)


Build
====
The project can be build using `gradle` with attached `gradlew`.